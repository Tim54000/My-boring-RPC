extern crate discord_rpc_sdk;

use std::time::SystemTime;
use discord_rpc_sdk::{EventHandlers, JoinRequest, JoinRequestReply, RichPresence, RPC};
use std::thread::sleep;
use std::time::Duration;

const APPLICATION_ID: &'static str = "440794695359266826";

struct Handlers;

impl EventHandlers for Handlers {
    fn ready() {
        println!("We're ready!");
    }

    fn errored(errcode: i32, message: &str) {
        println!("Error {}: {}", errcode, message);
    }

    fn disconnected(errcode: i32, message: &str) {
        println!("Disconnected {}: {}", errcode, message);
    }

    fn join_game(secret: &str) {
        println!("Joining {}", secret);
    }

    fn spectate_game(secret: &str) {
        println!("Spectating {}", secret);
    }

    fn join_request<R: FnOnce(JoinRequestReply)>(request: JoinRequest, respond: R) {
        println!("Join request from {:?}", request);
        respond(JoinRequestReply::Yes);
    }
}

fn main() {
    let rpc = RPC::init::<Handlers>(APPLICATION_ID, true, None).unwrap();
    let mut sub = 0;
    let mut presence = RichPresence {
        details: None,
        state: Some("Recherche ses abonnés".to_string()),
        start_time: Some(SystemTime::now()),
        large_image_key: Some("boring".to_string()),
        large_image_text: Some("S'ennuie".to_string()),
        small_image_key: Some("youtube".to_string()),
        small_image_text: None,
        party_size: Some(sub),
        party_max: Some(155),
        spectate_secret: None,
        join_secret: None,
        ..Default::default()
    };
    rpc.update_presence(presence.clone()).unwrap();

    loop {
        sleep(Duration::new(15,0));
        sub+=1;
        presence.party_size = Some(sub);
        rpc.update_presence(presence.clone()).unwrap();
    }
}